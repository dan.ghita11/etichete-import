Ce ai nevoie sa downlaodezi doar ca sa rulezi:
- Git
- NodeJs

Extra de download:
- Visual Studio Code

Ca sa clonezi local va trebui sa rulezi in Git Bash (dupa ce instalezi git):
- `git clone https://gitlab.com/dan.ghita11/etichete-import.git`

Comenzi dupa ce dai `git clone`:
- `npm install`
- `npm install testcafe -g`
- `testcafe chrome etichete-gs1.js`

Rulezi comenzile din Click Dreapta > Git Bash **SAU** din terminal vsc cu **Git Bash** setat ca terminal default shell

Ca sa adaugi modificarile tale in cloud:
- `git add --all`
- `git commit -m 'mesaj'`
- `git push origin master`

Daca iti da eroare de git Authentication Failed atunci trebuie sa iti generezi local un SSH key si sa il adaugi pe GitLab:
- Click pe iconita profilului tau de GitLab in dreapta sus (pe dropdown arrow)
- Settings
- SSH Keys
- local vei rula din Git Bash: `ssh-keygen -t rsa -b 4096 -C "email@example.com"`
- dai enter la toate intrebarile ca se fie by default
- copiezi ce scrie in `id_rsa.pub` in GitLab ca un SSH key nou