import { Selector, t } from 'testcafe'
import Helper from './helper'

let helper = new Helper()
let xlsx = require('node-xlsx')
let obj = xlsx.parse(`${__dirname}/Book1.xlsx`)
const faker = require('faker')
const ambalajOmogen = require('./statice/ambalaj-omogen.json')
const produsDeBaza = require('./statice/produs-de-baza.json')
const loginSite = 'https://codaloc.gs1.ro/codaloc/index.jsf'
let excel = {
    denumire: [],
    tipImpachetare: [],
    carton: [],
    cantitate: [],
    categorie: []
}

const parseMyExcel = async () => {
    for(let i = 1; i < obj[0].data.length; i++) {
        let x = obj[0].data[i] // obj[0] - 0 este indexul sheet-ului, data[i] - i este indexul coloanei
        excel.denumire.push(x[0])
        excel.tipImpachetare.push(x[1])
        excel.carton.push(x[2])
        excel.cantitate.push(x[3])
        excel.categorie.push(x[4])
    }
}

const login = async () => {
    await t.click(Selector('a').withText('Login'))
    await t.typeText(Selector('input').withAttribute('id', 'username'), '5949064199990')
    await t.typeText(Selector('input').withAttribute('id', 'password'), 'TYRON90641')
    await t.click(Selector('input').withAttribute('id', 'kc-login'))
}

fixture`bla_bla`.beforeEach(async t => {
    await t.navigateTo(loginSite)
    await t.maximizeWindow()

    await login()
})

const cerereCodProdusDeBaza = async produs => {
    console.log('produs', produs)
    await t.click(Selector('a').withAttribute('id', 'tp1'))
    await t.click(Selector('a').withAttribute('id', 'fm:ac1'))

    // denumire
    await t.typeText(Selector('input').withAttribute('id', 'fm:detaliiComune:denumireLunga'), produs.denumire)
    // descriere produs
    await t.typeText(Selector('input').withAttribute('id', 'fm:detaliiComune:denumireScurta'), produs.denumire)
    // descriere eticheta
    await t.typeText(Selector('textarea').withAttribute('id', 'fm:detaliiComune:descriere'), produs.denumire)

    // brands
    await t.click(Selector('select').withAttribute('id', 'fm:detaliiAmbalare:tipBrand'))
    await t.click(Selector('option').withAttribute('value', produsDeBaza.brandOption))
    await t.typeText(
        Selector('div').withAttribute('id', 'fm:detaliiAmbalare:marca')
        .find('input'),
        produsDeBaza.brandValue
    )

    // tip material de ambalare
    await helper.selectFromDropdown('fm:detaliiAmbalare:materialAmbalare', produsDeBaza.tipMaterialDeAmbalare, produs.tipImpachetare)

    // tip ambalaj
    await helper.selectFromDropdown('fm:detaliiAmbalare:DenumireAmbalaj', produsDeBaza.tipAmbalaj)

    // cantitate neta
    await t.typeText(Selector('input').withAttribute('id', 'fm:detaliiAmbalare:greutatea_neta'), produsDeBaza.cantitateNeta)
    await helper.selectFromDropdown('fm:detaliiAmbalare:um_greutatea_neta', produsDeBaza.cantitateTip)

    // checkbox imi asum intreaga..
    await t.click(Selector('input').withAttribute('name', /^fm:asumare:/))
}

const cerereAmbalajOmogen = async codGin => {
    //todo: add code
}

test('adaugare produse de baza', async t => {
    await parseMyExcel()

    for(let i = 0; i < excel.denumire.length; i++) {
        let produs = {
            denumire: excel.denumire[i],
            tipImpachetare: excel.tipImpachetare[i],
            carton: excel.carton[i],
            cantitate: excel.cantitate[i],
            categorie: excel.categorie[i]
        }
        await cerereCodProdusDeBaza(produs)
        //todo: cauta cod gin
        //          -> await Selector('ceva').innerText
        //todo: await cerereAmbalajOmogen(codGin)
        await t.wait(15000)
    }
})


