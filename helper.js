import { Selector, t } from 'testcafe'

export default class Helper {
    async selectFromDropdown (idDropdown, optionText) {
        const selector = Selector('select').withAttribute('id', idDropdown)
        await t.click(selector)
        await t.wait(500)
        await t.click(selector.find('option').withText(optionText))
    }

    async selectFromDropdown (idDropdown, defaultOptionText, customOptionText) {
        let optionText = customOptionText ? customOptionText : defaultOptionText

        const selector = Selector('select').withAttribute('id', idDropdown)
        await t.click(selector)
        await t.wait(500)
        await t.click(selector.find('option').withText(optionText))
    }
}